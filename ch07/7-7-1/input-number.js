Vue.component('input-number', {
    template: '\
        <div class="input-number">\
            <input \
                type="text"\
                :value="currentValue"\
                @change="handleChange"\
                @keyup.up="handleUp(0)"\
                @keyup.down="handleDown(0)"/>\
            <button \
                @click="handleDown(1)"\
                :disabled="currentValue <= min">-</button>\
            <button \
                @click="handleUp(1)"\
                :disabled="currentValue >= max">+</button>\
            <label>Step: </label>\
            <input \
                type="text"\
                :value="currentStep"\
                @change="handleStepChange">\
        </div>',
    props: {
        max: {
            type: Number,
            default: Infinity
        },
        min: {
            type: Number,
            default: -Infinity
        },
        value: {
            type: Number,
            default: 0
        },
        step: {
            type: Number,
            default: 1
        }
    },
    data: function () {
        return {
            // 在元件內部維護這個變數
            currentValue: this.value,
            currentStep: this.step
        }
    },
    watch: {
        currentValue: function (val) {
            this.$emit('input', val);       // v-model
            this.$emit('on-change', val);   // change
        },
        value: function (val) {
            // 父元件修改
            this.updateValue(val);
        }
    },
    methods: {
        handleDown: function (control) {
            if (this.currentValue <= this.min) return;
            if (control === 0) {
                this.currentValue -= 1;
            } else if (control === 1) {
                this.currentValue -= (1 * this.currentStep);
            }
        },
        handleUp: function (control) {
            if (this.currentValue >= this.max) return;
            if (control === 0) {
                this.currentValue += 1;
            } else if (control === 1) {
                this.currentValue += (1 * this.currentStep);
            }
        },
        handleChange: function (event) {
            var val = event.target.value.trim();
            var max = this.max;
            var min = this.min;

            if (isValueNumber(val)) {
                val = Number(val);
                this.currentValue = val;
                if (val > max) {
                    this.currentValue = max;
                } else if (val < min) {
                    this.currentValue = min;
                }
            } else {
                event.target.value = this.currentValue;
            }
        },
        handleStepChange: function(event) {
            var val = event.target.value.trim();
            if (isValueNumber(val)) {
                val = Number(val);
                this.currentStep = val;
            } else {
                event.target.value = this.currentStep;
            }
        },
        updateValue: function (val) {
            if (val > this.max) val = this.max;
            if (val < this.min) val = this.min;
            this.currentValue = val;
        }
    },
    mounted: function () {
        // init
        this.updateValue(this.value);
    }
});

function isValueNumber(value) {
    return (/(^-?[0-9]+\.{1}\d+$)|(^-?[1-9][0-9]*$)|(^-?0{1}$)/).test(value + '');
}