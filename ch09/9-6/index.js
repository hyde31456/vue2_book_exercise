var app = new Vue({
    el: '#app',
    data: {
        username: '',
        message: '',
        list: []
    },
    methods: {
        handleSend: function () {
            // 送出留言前檢查是否為空
            if (this.username === '') {
                window.alert('請輸入暱稱');
                return;
            }
            if (this.message === '') {
                window.alert('請輸入留言')
                return;
            }
            this.list.push({
                name: this.username,
                message: this.message
            });
            this.message = '';
        },
        handleReply: function (index) {
            let name = this.list[index].name;
            this.message = '回覆@' + name + ': ';
            // 回覆後 focus 在 textarea
            this.$refs.message.focus();
        },
        handleDelete: function(index) {
            this.list.splice(index, 1);
        }
    },
});