Vue.component('vInput', {
    props: {
        value: {
            type: [String, Number],
            default: ''
        }
    },
    render(h) {
        var _this = this;

        return h('div', [
            h('span', '暱稱：'),
            h('input', {
                attrs: {
                    type: 'text'
                },
                domProps: {
                    value: this.value
                },
                on: {
                    input: function (e) {
                        _this.value = e.target.value
                        _this.$emit('input', e.target.value); // 透過 $emit('input') 將輸入的內容傳送給父元件
                    }
                }
            }),

        ]);
    }
});

Vue.component('vTextarea', {
    props: {
        value: {
            type: String,
            default: ''
        }
    },
    render(h) {
        var _this = this;

        return h('div', [
            h('span', '留言內容：'),
            h('textarea', {
                attrs: {
                    placeholder: '請輸入留言內容'
                },
                domProps: {
                    value: this.value
                },
                ref: 'message',
                on: {
                    input: function (e) {
                        _this.value = e.target.value
                        _this.$emit('input', e.target.value); // 透過 $emit('input') 將輸入的內容傳送給父元件
                    }
                }
            }),

        ]);
    },
    methods: {
        focus: function() {
            this.$refs.message.focus();
        }
    },
});