Vue.component('vList', {
    props: {
        list: {
            type: Array,
            default: function () {
                return [];
            }
        }
    },
    render(h) {
        var _this = this;
        var list = [];

        this.list.forEach((msg, index) => {

            let node = h('div', {
                class: {
                    'list-item': true
                }
            }, [
                h('span', msg.name + ': '),
                h('div', {
                    class: {
                        'list-msg': true
                    }
                }, [
                    h('p', msg.message),
                    h('a', {
                        class: {
                            'list-delete': true
                        },
                        style: {
                            color: '#9f1f1f'
                        },
                        on: {
                            click: function () {
                                _this.handleDelete(index);
                            }
                        }
                    }, '刪除'),
                    h('a', {
                        class: {
                            'list-reply': true
                        },
                        on: {
                            click: function () {
                                _this.handleReply(index);
                            }
                        }
                    }, '回覆')
                ])
            ]);

            list.push(node);

        });

        if (this.list.length) {
            return h('div', {
                class: {
                    list: true
                }
            }, list)
        } else {
            return h('div', {
                class: {
                    'list-nothing': true
                }
            }, '留言內容是空的')
        }
    },
    methods: {
        handleReply: function (index) {
            this.$emit('reply', index);
        },
        handleDelete: function (index) {
            this.$emit('delete', index);
        }
    }
});