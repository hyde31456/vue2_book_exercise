Vue.component('vInput', {
    template: '\
        <div>\
            <span>暱稱：</span>\
            <input \
                type="text" \
                :value="value" \
                @input="handleInput">\
        </div>'
    ,
    props: {
        value: {
            type: [String, Number],
            default: ''
        }
    },
    methods: {
        handleInput: function(e) {
            this.value = e.target.value;
            this.$emit('input', e.target.value); // 透過 $emit('input') 將輸入的內容傳送給父元件
        }
    }
});

Vue.component('vTextarea', {
    template: '\
        <div>\
            <span>留言內容：</span>\
            <textarea \
                placeholder="請輸入留言內容" \
                :value="value" \
                @input="handleInput"\
                ref="message">\
        </div>'
    ,
    props: {
        value: {
            type: String,
            default: ''
        }
    },
    methods: {
        focus: function() {
            this.$refs.message.focus();
        },
        handleInput: function(e) {
            this.value = e.target.value;
            this.$emit('input', e.target.value); // 透過 $emit('input') 將輸入的內容傳送給父元件
        }
    },
});