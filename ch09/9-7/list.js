Vue.component('vList', {
    template: '\
        <div v-if="list.length" class="list">\
            <div v-for="(msg, index) in list" class="list-item">\
                <span>{{ msg.name }}</span>\
                <div class="list-msg">\
                    <p>{{ msg.message }}</p>\
                    <a class="list-delete" style="color: #9f1f1f" @click="handleDelete(index)">刪除</a>\
                    <a class="list-reply" @click="handleReply(index)">回覆</a>\
                </div>\
            </div>\
        </div>\
        <div v-else class="list-nothing">留言內容是空的</div>',
    props: {
        list: {
            type: Array,
            default: function () {
                return [];
            }
        }
    },
    methods: {
        handleReply: function (index) {
            this.$emit('reply', index);
        },
        handleDelete: function (index) {
            this.$emit('delete', index);
        }
    }
});