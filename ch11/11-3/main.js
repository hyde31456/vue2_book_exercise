import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuex from 'vuex';
import App from './app.vue';
import VueBus from './vue-bus';
import VueAjax from './vue-ajax';

Vue.use(VueRouter); // 使用 plugin
Vue.use(Vuex);
Vue.use(VueBus); // 使用 Vue Bus
Vue.use(VueAjax);

// 建立一個陣列來制定路由
// webpack 會把每一個路由都打包成一個 js 檔案，再請求到該頁面時才會加載這個頁面的 js 檔案(按需加載)。
const Routers = [{
        path: '*', // 當存取到的路徑不存在時，導回首頁。
        redirect: '/index'
    },
    {
        path: '/index', // 指定當前對應的路徑
        meta: {
            title: '首頁'
        },
        component: (resolve) => require(['./views/index.vue'], resolve) // 對應的組件
        // component: require('./views/index.vue') 一次性載入
    },
    {
        path: '/about',
        meta: {
            title: '關於'
        },
        component: (resolve) => require(['./views/about.vue'], resolve)
    }
];

const RouterConfig = {
    // 使用 HTML5 的 History 路由模式，透過 / 設置路徑。
    mode: 'history',
    routes: Routers
};

const router = new VueRouter(RouterConfig);
router.beforeEach((to, from, next) => {
    // to: 即將要進入的目標、路由物件
    // from: 當前導航要離開的路由物件
    // next: 呼叫該方法後才能進入下一個鉤子 link

    // if(window.localStorage.getItem('token')) {
    window.document.title = to.meta.title;
    next();
    // } else {
    //     next('/login');
    // }

});
router.afterEach((to, from, next) => {
    window.scrollTo(0, 0); // 點擊進入下一頁時回到頁面最上方
});

// Vuex 設置
const moduleA = {
    state: {},
    mutations: {},
    actions: {},
    getters: {}
};

const moduleB = {
    state: {},
    mutations: {},
    actions: {},
    getters: {}
};

const store = new Vuex.Store({
    state: {
        count: 0,
        list: [1, 5, 8, 10, 30, 50]
    },
    mutations: {
        // 用來改變 store 的 model
        increment(state, params) {
            state.count += params.count;
        },
        decrease(state, n = 1) {
            state.count -= n;
        },
        increase(state, n = 1) {
            state.count += n;
        },
    },
    getters: { // 每個元件都可以使用此 model
        filteredList: state => {
            return state.list.filter(item => item < 10)
        },
        listCount: (state, getters) => {
            return getters.filteredList.length;
        }
    },
    actions: {
        decrease(context) {
            context.commit('decrease');
        },
        asyncIncrease(context) {
            return new Promise(resolve => {
                setTimeout(() => {
                    context.commit('increase');
                    resolve();
                }, 1000)
            });
        }
    }
});

new Vue({
    el: '#app',
    router: router,
    store: store,
    render: h => h(App)
});