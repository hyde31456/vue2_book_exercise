var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin'); // 產生 html 檔案
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var merge = require('webpack-merge'); // 合併兩分 webpack.config.js 檔案
var webpackBaseConfig = require('./webpack.config.js');
var UglifyJsPlugin = require('uglifyjs-webpack-plugin');

// 清除基本設置的插件清單
webpackBaseConfig.plugins = [];

module.exports = merge(webpackBaseConfig, {
    output: {
        publicPath: '/dist/',
        filename: '[name].[hash].js' // 將入口文件命名為帶有 20 位 hash 值得唯一檔案
    },
    plugins: [
        new ExtractTextPlugin({
            filename: '[name].[hash].css',
            allChunks: true
        }),
        // 定義當前 node 環境為產品環境
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: '"production"'
            }
        }),
        // 取出模板，保存入口 html 檔案
        new HtmlWebpackPlugin({
            filename: '../index_prod.html',
            template: './index.ejs', // ejs 是 JS 模板庫，從 JSON 資料中產生 HTML 字串。
            inject: false
        })
    ],
    // 壓縮
    optimization: {
        minimizer: [
            new UglifyJsPlugin({
                uglifyOptions: {
                    compress: false
                }
            })
        ]
    },
})