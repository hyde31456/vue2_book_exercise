// vue bus plugin
const install = function (Vue) {
    // Bus 有三個方法 $emit、$on、$off
    const Bus = new Vue({
        methods: {
            // ...args 因不確定會接收到多少個參數，使用 ...args 來全部獲取到。
            emit(event, ...args) {
                this.$emit(event, ...args);
            },
            on(event, callback) {
                this.$on(event, callback);
            },
            off(event, callback) {
                this.$off(event, callback);
            }
        },
    });
    Vue.prototype.$bus = Bus;
}
export default install;