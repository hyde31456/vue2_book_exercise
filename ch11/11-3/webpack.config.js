var path = require('path');

// 引入 plugins
var ExtractTextPlugin = require('extract-text-webpack-plugin');

var config = {
    // webpack 從哪裡開始尋找 dependencies
    entry: {
        main: './main'
    },
    // 編譯後文件儲存位置與名稱
    output: {
        path: path.join(__dirname, './dist'), // 打包後文件的輸出目錄 demo/dist/main.js
        publicPath: '/dist/', // 指定資源文件引用的目錄
        filename: 'main.js', // 輸出的文件名稱
        chunkFilename: '[name].chunk.js' // 編譯出的每個月面的 js 名稱
    },
    module: {
        rules: [ // 可以指定一系列的 loaders，每一個 loader都必須包含 test、use 兩個 options。
            {
                test: /\.(gif|jpg|png|woff|svg|eot|ttf)\??.*$/,
                loader: 'url-loader?limit=1024' // 小於1kb不會生成檔案，以base64形式加載。
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: {
                    loaders: {
                        css: ExtractTextPlugin.extract({
                            use: 'css-loader',
                            fallback: 'vue-style-loader'
                        })
                    }
                }
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            },
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    use: 'css-loader',
                    fallback: 'style-loader'
                })
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin("main.css") // 重新命名提取後的 css 文件
    ]
};

module.exports = config; // export default config;