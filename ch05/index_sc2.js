
var app = new Vue({
    el: '#app',
    data: {
        selectCategory: -1,
        selectItem: -1,
        list:[
                [
                    {
                        id: 1,
                        name: 'iPhone 7',
                        category: '電子產品',
                        price: 6188,
                        count: 0
                    },
                    {
                        id: 2,
                        name: 'iPad Pro',
                        category: '電子產品',
                        price: 5888,
                        count: 0
                    },
                    {
                        id: 3,
                        name: 'MacBook Pro',
                        category: '電子產品',
                        price: 21488,
                        count: 0
                    }
                ],
                [
                    {
                        id: 4,
                        name: '水',
                        category: '日常用品',
                        price: 10,
                        count: 0
                    },
                    {
                        id: 5,
                        name: '衛生紙',
                        category: '日常用品',
                        price: 120,
                        count: 0
                    }
                ],
                [
                    {
                        id: 6,
                        name: '柚子',
                        category: '蔬果',
                        price: 10,
                        count: 0
                    },
                    {
                        id: 7,
                        name: '檸檬',
                        category: '蔬果',
                        price: 5,
                        count: 0
                    }
                ]
        ]
    },
    methods: {
        handleReduce: function(index, indexs) {
            if(this.list[index][indexs].count === 0) return;
            this.list[index][indexs].count--;
        },
        handleAdd: function(index, indexs) {
            this.list[index][indexs].count++;
        },
        handleRemove: function(index, indexs) {
            this.list[index].splice(indexs, 1); // 分割
            this.handleSelectAll();
        },
        handleSelect: function(index, indexs) {
            this.selectCategory = index;
            this.selectItem = indexs;
        },
        handleSelectAll: function() {
            this.selectCategory = -1;
            this.selectItem = -1;
        }
    },
    computed: {
        totalPrice: function() {
            var total = 0;
            var _selectCategory = this.selectCategory;
            var _selectItem = this.selectItem;
            if(_selectCategory === -1 && _selectItem === -1) {
                for(var i=0; i<this.list.length; i++) {
                    for(var j=0; j<this.list[i].length; j++) {
                        var item = this.list[i][j];
                        total += item.price * item.count;
                    }
                }
            } else {
                total += this.list[_selectCategory][_selectItem].price * this.list[_selectCategory][_selectItem].count;
            }
            return total.toString().replace(/\B(?=(\d{3})+$)/g, ',') ;
        },
        isListEmpty: function() {
            var length = 0;
            for(var i=0; i<this.list.length; i++) {
                for(var j=0; j<this.list[i].length; j++) {
                    length++;
                }
            }
            return length;
        }
    }
})